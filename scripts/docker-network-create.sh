#!/bin/bash
set -e
TIME=date\ +%d/%m/%Y-%H:%M:%S
CURRENT_DIR=$(dirname "$0")
cd ${CURRENT_DIR}
clear

# /docker_projects/ldap-server/scripts/docker-network-create.sh

echo [$(${TIME})] - EXECUTE DOCKER COMPOSE UP
echo DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
echo [$(${TIME})] - CURRENT DIRECTORY: ${CURRENT_DIR};

export $(cat ../.env | grep NETWORK_NAME)
docker network create -d bridge ${NETWORK_NAME}