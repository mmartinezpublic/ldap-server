#!/bin/bash
set -e
TIME=date\ +%d/%m/%Y-%H:%M:%S
CURRENT_DIR=$(dirname "$0")
cd ${CURRENT_DIR}
clear

# /docker_projects/ldap-server/scripts/docker-compose-clean.sh

cd ..
CURRENT_DIR=$(pwd)
echo [$(${TIME})] - EXECUTE DOCKER COMPOSE UP
echo DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
echo [$(${TIME})] - CURRENT DIRECTORY: ${CURRENT_DIR};

read -rsp $'Press enter to continue...\n'
docker-compose ps
docker-compose rm -v