@ECHO OFF
cls

ECHO. [%time%] - EXECUTE DOCKER COMPOSE CLEAN
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

CD /D %CURRENT_DIR%
CD..

RD /S /Q  %CD%\volumen\slapd\database
MKDIR %CD%\volumen\slapd\database

RD /S /Q  %CD%\volumen\slapd\certs
MKDIR %CD%\volumen\slapd\certs

RD /S /Q  %CD%\volumen\slapd\config
MKDIR %CD%\volumen\slapd\config


PAUSE