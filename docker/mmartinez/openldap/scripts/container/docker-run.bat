@ECHO OFF
cls

ECHO. [%time%] - EXECUTE RUN DOCKER CONTAINER
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

CD /D %CURRENT_DIR%
CD..
SET CURRENT_DIR=%CD%\

FOR /F "tokens=*" %%G  IN (%CURRENT_DIR%config\process-name.txt) DO (
  SET PROCESS_NAME=%%G
)

FOR /F "tokens=*" %%G  IN (%CURRENT_DIR%config\image-name.txt) DO (
  ECHO.DOCKER RUN %%G
  docker run --cidfile "%CURRENT_DIR%cid" -d --name %PROCESS_NAME% -p 389:389 -e SLAPD_PASSWORD=mysecretpassword -e SLAPD_DOMAIN=example.org -e INITIAL_ADMIN_USER=admin -e INITIAL_ADMIN_PASSWORD=mysecretpassword -e SLAPD_FORCE_RECONFIGURE=true -e SLAPD_FULL_DOMAIN=dc=example,dc=org -e SLAPD_CONFIG_PASSWORD=config-secret %%G
  ECHO.START - ERRORLEVEL: %ERRORLEVEL%
)

PAUSE