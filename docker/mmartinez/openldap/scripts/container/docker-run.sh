#!/bin/bash
set -e
TIME=date\ +%d/%m/%Y-%H:%M:%S
CURRENT_DIR=$(dirname "$0")
cd ${CURRENT_DIR}
clear

# /docker_projects/ldap-server/docker/mmartinez/openldap/scripts/container/docker-run.sh
cd ..
cd ..
CURRENT_DIR=$(pwd)

echo [$(${TIME})] - EXECUTE DOCKER COMPOSE UP
echo DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
echo [$(${TIME})] - CURRENT DIRECTORY: ${CURRENT_DIR};

I=$(cat ./scripts/config/image-name.txt) 

P=$(cat ./scripts/config/process-name.txt) 
docker run --cidfile "${CURRENT_DIR}cid" --name ${P} -p 389:389 -e "--loglevel debug" -e SLAPD_PASSWORD=mysecretpassword -e SLAPD_DOMAIN=example.org -e INITIAL_ADMIN_USER=admin -e INITIAL_ADMIN_PASSWORD=mysecretpassword -e SLAPD_FORCE_RECONFIGURE=true -e SLAPD_FULL_DOMAIN=dc=example,dc=org -e SLAPD_CONFIG_PASSWORD=config-secret ${I}

docker run --privileged=true --name someldap -p 389:389 -e SLAPD_PASSWORD=mysecretpassword -e SLAPD_DOMAIN=example.org -e INITIAL_ADMIN_USER=admin -e INITIAL_ADMIN_PASSWORD=mysecretpassword -e SLAPD_FORCE_RECONFIGURE=true -e SLAPD_FULL_DOMAIN=dc=example,dc=org -e SLAPD_CONFIG_PASSWORD=config-secret osixia/openldap:1.1.4

docker run --rm --privileged=true -P osixia/openldap --copy-service