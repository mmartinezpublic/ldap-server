#!/bin/bash
set -e
TIME=date\ +%d/%m/%Y-%H:%M:%S
CURRENT_DIR=$(dirname "$0")
cd ${CURRENT_DIR}
clear

# /docker_projects/ldap-server/docker/mmartinez/openldap/scripts/container/docker-build.sh
cd ..
cd ..
CURRENT_DIR=$(pwd)

echo [$(${TIME})] - EXECUTE DOCKER COMPOSE UP
echo DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
echo [$(${TIME})] - CURRENT DIRECTORY: ${CURRENT_DIR};

G=$(cat ./scripts/config/image-name.txt) 
docker build -t ${G} .